# FSFE Mailman Templates

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/mailman-templates/00_README)

This repository contains the default templates we use for our lists (in `templates/`) and some custom ones for specific lists (in `lists/`).

## Translate custom FSFE templates

Most of the templates in the `templates/` folder are Mailman defaults, but some important ones have been changed by us to look nicer and/or to contain additional information.

- [listinfo.html](https://git.fsfe.org/fsfe-system-hackers/mailman-templates/src/branch/master/templates/en/listinfo.html)
- [subscribe.html](https://git.fsfe.org/fsfe-system-hackers/mailman-templates/src/branch/master/templates/en/subscribe.html)
- [subscribeack.txt](https://git.fsfe.org/fsfe-system-hackers/mailman-templates/src/branch/master/templates/en/subscribeack.txt)

Under `lists/`, you will find templates for some special mailing lists like the newsletter. You can translate everything in the `en` folder and put your contribution under a separate folder named after your language code.

If you intent to translate them (which is highly welcome!), please note:

- You may take some of the texts from the already existing translations.
- In the HTML files, you may copy a lot from fsfe.org.
- In the custom HTML files, please note the comment in the first line stating the revision, e.g. `<!-- fsfe rev 1 -->`. This may help you to find out whether the file in your language is on the same state content-wise as the English original.

Thank you for your contribution!

## Deployment

The template folder of this repository is symlinked: `/var/lib/mailman/templates -> /etc/mailman/mailman-templates-git/templates`.

For the custom list templates, these are symlinked as well, for example: `/var/lib/mailman/lists/newsletter-en/en -> /etc/mailman/mailman-templates-git/lists/newsletter-all/en`. For a newly added language, this can be done by (exmaple for nl (Dutch), so replace for new language code):

```
for list in /var/lib/mailman/lists/newsletter-*; do 
  ln -s /etc/mailman/mailman-templates-git/lists/newsletter-all/nl $list/nl
done
```

## Custom changes necessary for deployment

Our custom templates require some server-side changes in order to make them work.

### Enable CORS

In Apache config for fsfe.org, add

```
# Allow CORS for fsfe.org domains
SetEnvIf Origin ^(https?://.+\.fsfe\.org(?::\d{1,5})?)$   CORS_ALLOW_ORIGIN=$1
Header append Access-Control-Allow-Origin  %{CORS_ALLOW_ORIGIN}e   env=CORS_ALLOW_ORIGIN
Header merge  Vary "Origin"
```

### Loosen Mailman HTML blocklist

In `/usr/lib/mailman/Mailman/Utils.py`, add to `_badwords = [`:

```
'<link(?! href="https://fsfe.org/.*")',
```
