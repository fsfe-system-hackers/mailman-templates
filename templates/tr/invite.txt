"%(email)s" e-posta adresiniz, %(hostname)s üzerinde bulunan
%(listname)s listesine üye olması için %(listname)s liste yöneticisi
tarafından davet edildi. Daveti, Konu: başlığını değiştirmeden
bu mesajı yanıtlayarak kabul edebilirsiniz. 

Ayrıca bu sayfayı da ziyaret edebilirsiniz:

    %(confirmurl)s


Veya bir mesaja aşağıdaki satırı -- sadece aşağıdaki satırı --
yazarak %(requestaddr)s adresine gönderin:

    confirm %(cookie)s

Basitçe bu mesajı `yanıtlamak', çoğu e-posta okuma programında
çalışır.

Bu daveti reddetmek istiyorsanız, lütfen bu mesajı gözardı edin.
Eğer herhangi bir sorunuz varsa, lütfen %(listowner)s ile
bağlantı kurun.

