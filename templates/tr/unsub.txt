%(listname)s mesaj listesi için listeden çıkma onay bildirimi

%(remote)s, "%(email)s" olan e-posta adresinizin çıkarılması için bir
istek aldık. Bu listeden çıkmak istediğinizi onaylamak için bu
mesajı, Konu: başlığını değiştirmeden yanıtlayın. Veya bu web
sayfasına gidin:

    %(confirmurl)s

Veya bir mesaja aşağıdaki satırı -- sadece aşağıdaki satırı --
yazarak %(requestaddr)s adresine gönderin:

    confirm %(cookie)s

Basitçe bu mesajı `yanıtlamak', çoğu e-posta okuma programında
çalışır, çünkü bu durumda Konu: satırı doğru biçimde kalır
(Konu: içinde fazladan "Re:" veya "Ynt:" olması sorun değildir).

Eğer bu listeden çıkmak istemiyorsanız, bu mesajı gözardı edin.
Eğer bu listeden kötü niyetle çıkarılmak istediğinizi
düşünüyorsanız veya başka sorularınız varsa, bunları %(listadmin)s
adresine gönderin.

