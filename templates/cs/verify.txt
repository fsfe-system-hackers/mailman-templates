Konference %(listname)s -- potvrzení přihlášení

Obdrželi jsme požadavek z %(remote)s na přihlášení adresy
"%(email)s", do konference %(listaddr)s. Pro potvrzení, že tento požadavek
pochází od Vás zašlete, prosíme, zprávu na adresu %(requestaddr)s
a buď: 

- ponechte řádek věc: (Subject:) beze změny (případné "Re:" doplněné
  automaticky poštovním klientem nevadí),

- nebo vložte do těla dopisu následující řádek. Kromě něj odstraňte
  jakýkoliv další text, řádek musí být na první řádce a začínat hned
  na začátku řádky.

confirm %(cookie)s

(Ve většině poštovních klientů prostě zašlete odpověď na tento dopis.
Obvykle je totiž řádek věc: ponechán beze změny.)

Nebo můžete místo zaslání mailu navštívit následující URL:

    %(confirmurl)s

Pokud se do konference přihlásit nechcete, nebo dokonce nevíte, proč jste
tento dopis dostali, prostě jej zahoďte. Pokud máte nějaký dotaz nebo
problém, prosím, kontaktujte správce na adrese: %(listadmin)s.
