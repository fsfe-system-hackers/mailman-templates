Toto je zkušební zpráva, můžete ji klidně ignorovat.

Konference %(listname)s má problémy s doručováním příspěvků na vaši,
adresu, což ukazuje, že doručování na adresu %(address)s nefunguje
správně.
Přikládáme ukázku vrácené zprávy, ze které poznáte, proč se zprávy vracejí.
Je možné, že tato zpráva pro vás není srozumitelná a budete muset o spolupráci
požádat správce svého poštovního systému. Ten by měl být schopen poznat,
k jakému problému dochází.

Pokud jste tuto zprávu obdrželi, zřejmě není problém s doručováním trvalý
a tak zůstáváte aktivním členem konference. Pokud by se nám nepodařilo
doručit ani tuto zprávu, vaše členství by bylo pozastaveno a nadále bychom
vám nezasílali příspěvky. Čas od času bychom pouze zkusili zastat zprávu
informující o pozastavení členství s pokyny pro jeho obnovení.

Na konfiguraci svého účtu se můžete podívat na adrese

    %(optionsurl)s

Na této stránce si můžete nastavit jinou emailovou adresu a nebo 
změnit parametry doručování.
Pokud máte k tomuto emailu dotaz, nebo si myslíte, že nebyl zaslán
správně, kontaktujte, prosím, správce konference na adrese:

    %(owneraddr)s
