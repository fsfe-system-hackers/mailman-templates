Este es un mensaje de prueba.  Puede ignorar este mensaje.

Mensajes enviados a usted desde la lista %(listname)s han sido rechazados en
varias ocasionnes, indicando que puede haber problemas con %(address)s.
Hemos adosado una muestra de estos rechazos.  Por favor examine este mensaje
para asegurarse que no nay problemas con su dirección de correo electrónico
(email). Puede contactar a su administrador de email si necesita más ayuda.

Si está leyendo este mensaje, no necesita hacer nada para permanecer en la lista. Si este mensaje hubiera sido devuelto usted no estaría leyendolo y se le habría desactivado como miembro de la lista. Normalmente, cuando se le desactiva, todavía tratamos de enviarle algunos mensajes y reactivar su subscripción.

Puede también visitar su página de suscriptor en 

    %(optionsurl)s

En esta página usted puede cambiar varias opciones relacionadas con la entrega
tales como su dirección de email, y si desea recibir sumarios o no. A modo de
recordatorio, su contraseña de suscripción es

    %(password)s

Si tiene alguna pregunta o problema que reportar, puede contactar al dueño de
la lista en

    %(owneraddr)s
