Tu dirección de correo electrónico "%(email)s" ha sido invitada a unirse a la
lista de distribución %(listname)s de %(hostname)s por su administrador.
Puede aceptar la invitación con solo responder a este mensjae manteniendo el
asunto intacto.

También puede visitar la página web:

    %(confirmurl)s

O puede incluir la línea siguiente -- y sólo la línea siguiente -- en un
mensaje dirigido a %(requestaddr)s:

    confirm %(cookie)s

Observe que la opción de responder debería de funcionar en la
mayoría de los programas de correo.

Si quiere declinar la invitación, basta con ignorar este mensaje.  Si
tiene alguna pregunta que formular, por favor, envíela a

%(listowner)s.
