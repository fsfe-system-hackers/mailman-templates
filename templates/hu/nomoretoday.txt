A(z) %(listname)s levelezőlista automatikus válaszokat küldő címére
a(z) `%(sender)s' címről ma már %(num)s üzenetet kaptunk.
Ezek az üzenetek, lehet hogy egymásra hivatkozó levelek
(mail loops), ezért a mai napon több automatikus válaszlevelet nem
küldünk el neked. Kérünk holnap próbálkozz újra. 

Ha úgy gondolod, hogy mi tévedtünk, vagy egyéb észrevételed van,
akkor írj a lista tulajdonosának a(z) %(owneremail)s címre. 