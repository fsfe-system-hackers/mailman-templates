A következő feliratkozási kérelem megerősítéséhez szükséges az 
engedélyed:

    Feladó:	%(username)s
    Lista:	%(listname)s@%(hostname)s

A kérelemről a(z) 

	%(admindb_url)s
	
címen tudsz állást foglalni.
