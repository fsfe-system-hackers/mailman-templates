Teie kiri listi '%(listname)s' teemal 

    %(subject)s

edastati listi moderaatorile läbivaatuseks.

Põhjus:

    %(reason)s

Olenevalt moderaatori otsusest kiri kas postitakse listi või
teavitatakse teid moderaatori otsusest. Postitust saab tühistada
aadressil

    %(confirmurl)s
