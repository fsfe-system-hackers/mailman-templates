Este é um lembrete, enviado de mês em mês, sobre os membros de 
sua lista de discussão em %(hostname)s. Isto inclui seus dados de 
inscrição e como usa-lo para modificar ou se desinscrever de uma 
lista.

Você pode visitar a URL para modificar seu status de membro ou 
configuração, incluindo desinscrição, ajustando a entrega 
no estilo digest ou desativando a entrega (e.g. para férias) e 
assim por diante.

Em adição a interface via URL, você também pode usar o email para 
fazer tais mudanças. Para mais detalhes, envie uma mensagem ao 
endereço "-request" da lista (por exemplo, %(exreq)s) contendo 
apenas a palavra 'help' no corpo da mensagem e uma mensagem 
de email será lhe enviada com instruções.

Se tiver questões, problemas, comentários, etc, envie-os para 
%(owner)s. Obrigado!

Senha para %(useraddr)s:

