Alguém, ou alguém se fazendo passar por você, solicitou o lembrete de 
senha para seu membro na lista de discussão %(fqdn_lname)s. Você 
precisará desta senha para modificar suas opções de membro (e.g. você 
prefere receber entregas regulares ou modo digest) e tendo esta senha 
torna simples a desinscrição da lista de discussão.

Você está inscrito com o endereço: %(user)s

Sua senha na lista %(listname)s é: %(password)s

Para fazer modificações em suas opções de membro, entre e visite
sua página web de opções:

    %(options_url)s

Você também poderá realizar tais modificações por email enviando
uma mensagem para:

    %(requestaddr)s

com o texto "help" no assunto ou corpo da mensagem. A resposta 
automática trará instruções mais detalhadas.

Questões ou comentários? Por favor envie-as para o administrador 
da lista de discussão %(listname)s no endereço %(owneraddr)s.
