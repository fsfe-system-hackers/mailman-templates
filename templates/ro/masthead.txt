TrimiteĹŁi mesajele adresate listei %(real_name)s la adresa
	%(got_list_email)s

Pentru a vÄ abona sau dezabona prin intermediul Internetului, vizitaĹŁi
	%(got_listinfo_url)s

sau, prin intermediul unui mesaj email: trimiteĹŁi un mesaj cu subiectul
sau mesajul 'help' la adresa
	%(got_request_email)s

PuteĹŁi apela persoana care administreazÄ lista la adresa
	%(got_owner_email)s

CĂ˘nd rÄspundeĹŁi, vÄ rugÄm sÄ editaĹŁi cĂ˘mpul Subiect pentru a fi cĂ˘t mai
specific, nu doar "Re: Contents of %(real_name)s digest..."
