Aceasta este o notificare de eĹec (bounce) a livrÄrii mesajelor listei de discuĹŁii:

    Lista:        %(listname)s
    Utilizatorul: %(addr)s
    Actiunea:     Abonamentul este ĂŽn prezent %(negative)s%(did)s.
    Motivul:      EĹecuri de livrare excesive sau fatale.s
    %(but)s

%(reenable)s
Detaliile sunt ataĹate mai jos.

AveĹŁi ĂŽntrebÄri?
ContactaĹŁi administratorul acestei liste la adresa %(owneraddr)s.