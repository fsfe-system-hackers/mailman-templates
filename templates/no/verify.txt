Bekreftelse på påmelding til listen %(listname)s

Vi har mottatt en forespørsel%(remote)s om å melde din epostadresse
"%(email)s" på listen %(listaddr)s. Dersom det er riktig at du ønsker
dette, send et svar på denne eposten, med samme emne som denne eposten
har. Eller gå inn på denne websiden:

    %(confirmurl)s

Du kan også sende en epost til %(requestaddr)s, med følgende (og KUN
følgende) innhold:

    confirm %(cookie)s

Merk at bare å sende et svar på denne eposten burde fungere fra de
fleste epostlesere, siden det vanligvis fører til at emnefeltet beholdes
(tillegg av 'Re:' eller 'SV:' foran emnet gjør ingenting).

Hvis du ikke ønsker å melde deg på listen, kan du bare slette denne eposten.
Tror du noen andre prøvde å melde deg på, eller har du andre spørsmål,
send gjerne en epost til %(listadmin)s.
