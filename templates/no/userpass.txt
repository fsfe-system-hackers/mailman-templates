Du, eller noen som utga seg for å være deg, har bedt om å få
tilsendt passordet for epostlisten %(fqdn_lname)s. Du trenger
dette passordet for å endre på dine innstillinger for listen
(f.eks. endre passord, få tilsendt sammendrag med jevne mellomrom
isteden, etc.). Passordet gjør det også lettere for deg å melde
deg av listen hvis det skulle bli aktuelt.

Du er medlem med epostadressen: %(user)s

Passordet ditt på epostlisten %(listname)s er: %(password)s

For å endre dine innstillinger for denne listen, logg inn på din
personlige webside for listen:

    %(options_url)s

Du har også mulighet til å gjøre disse endringer via epost ved å sende
en epost til:

    %(requestaddr)s

med ordet "help" som innhold eller i emnefeltet. Du vil da motta et
automatisk svar fra systemet med nærmere instruksjoner.

Har du spørsmål eller kommentarer? Kontakt listeadministratoren for
%(listname)s på %(owneraddr)s.
