Din forespørsel til %(requestaddr)s:

    %(cmd)s

er videresendt til personen ansvarlig for listen.

Det er nok fordi du prøver å melde deg på en 'lukket' liste.

Du fil få en epost med avgjørelsen så snart forespørselen din er
behandlet.

Spørsmål om krav og grunnlag for å få bli med på listen kan rettes til:

    %(adminaddr)s
