Bienvenue sur la liste %(real_name)s@%(host_name)s!

%(welcome)s

Pour écrire à la liste, envoyez votre message à :

  %(emailaddr)s

Cette liste de diffusion est régie par le Code de Conduite de la FSFE. 
Tous les participants sont priés d'adopter un comportement bienveillant 
les unes envers les autres : https://fsfe.org/about/codeofconduct

Les informations générales concernant cette liste sont à l'adresse :

  %(listinfo_url)s

Si vous souhaitez vous désabonner ou modifier vos options (par
exemple activer/désactiver l'envoi groupé, changer votre mot de
passe, etc.), consultez votre page d'abonnement à l'adresse :

  %(optionsurl)s
%(umbrella)s
Vous pouvez également procéder par mail en envoyant un message à:

  %(real_name)s-request@%(host_name)s 

avec le mot "help" dans le sujet ou le corps du message (sans les
guillemets), vous recevrez alors un message avec des instructions.

Votre mot de passe est nécessaire pour ces modifications (y compris
pour changer le mot de passe lui-même) ou pour résilier votre
abonnement.  Votre mot de passe est:

  %(password)s 

En principe, Mailman vous rappellera mensuellement vos mots de passe
pour les listes gérées sur %(host_name)s, rappel que vous pourrez
toutefois désactiver à votre convenance. Cet avis mensuel contiendra
également des instructions vous permettant de résilier votre
abonnement si vous le désirez ou de modifier vos options
d'abonnement. Vous trouverez un bouton dans la page de vos options
pour vous faire parvenir votre mot de passe par courriel.
