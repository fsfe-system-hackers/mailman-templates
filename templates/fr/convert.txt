La liste de diffusion %(listname)s vient de connaître un grand
changement.  Elle est maintenant gérée par un nouveau logiciel de
listes qui s'appelle "Mailman".  Avec un peu de chance, cela
corrigera beaucoup de problèmes qui ont été rencontrés lors de
l'administration de cette liste.

En quoi cela vous concerne-t-il? 

1) Le courrier destiné à la liste doit être envoyé à: %(listaddr)s. 

2) Il vous a été attribué un mot de passe généré automatiquement,
pour empêcher quiconque de vous désabonner à votre insu.  Il vous
sera envoyé dans un autre courriel séparé, que vous avez peut-être
déjà reçu.  Ne vous inquiétez pas si vous oubliez ce mot de passe,
un rappel mensuel vous sera envoyé par courriel.

3) Si vous avez accès au web, vous pouvez vous en servir à tout moment
pour vous désabonner, choisir de recevoir les messages en mode groupé
ou non, consulter les archives de la liste (qui seront disponibles
après qu'il y ait eu des messages dans un jour ou deux),
etc. L'adresse web pour cette interface est:

     %(listinfo_url)s 

4) Si vous n'avez pas accès au web, vous pourrez effectuer les mêmes
opérations par courriel.  Envoyez un message à %(requestaddr)s avec le
mot "help" (sans les guillemets) dans le sujet ou le corps du
message. Vous recevrez alors une réponse automatique vous expliquant
la marche à suivre.

Pour toute question ou problème concernant cette nouvelle
configuration, envoyez un courriel à l'adresse:
%(adminaddr)s. 

Ce message a été auto-généré par Mailman %(version)s. Pour plus
d'informations sur le logiciel Mailman, consultez le site Internet
de Mailman à l'adresse http://www.list.org/
