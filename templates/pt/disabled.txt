A sua participação na lista %(listname)s foi desactivada %(reason)s.
Não receberá mais mensagens desta lista até reactivar a sua inscrição.
Receberá mais %(noticesleft)s mensagens como este antes de a sua 
inscrição na lista ser apagada.

Para reactivar a sua participação pode simplesmente responder a esta
mensagem (deixando a linha Assunto: intacta), ou visitar a página de
confirmação em

    %(confirmurl)s

Pode também visitar a sua página de membro em

    %(optionsurl)s

Aí pode mudar várias opções, tais como o seu endereço e o modo de entrega
das mensagens. Lembra-se que a sua password de membro é

    %(password)s

Se tiver quaisquer perguntas ou problema pode contactar o dono da lista em

    %(owneraddr)s
